FOX's Book of Martyrs
=====================

Chapter 1: History of Christian Martyrs to the First General Persecutions
-------------------------------------------------------------------------

### St. Stephen

> And Stephen, full of grace and power, was doing great wonders and signs among the people. Then some of those who belonged to the synagogue of the Freedmen (as it was called), and of the Cyrenians, and of the Alexandrians, and of those from Cilicia and Asia, rose up and disputed with Stephen. But they could not withstand the wisdom and the Spirit with which he was speaking. Then they secretly instigated men who said, "We have heard him speak blasphemous words against Moses and God." And they stirred up the people and the elders and the scribes, and they came upon him and seized him and brought him before the council, and they set up false witnesses who said, "This man never ceases to speak words against this holy place and the law, for we have heard him say that this Jesus of Nazareth will destroy this place and will change the customs that Moses delivered to us." And gazing at him, all who sat in the council saw that his face was like the face of an angel. And the high priest said, "Are these things so?" And Stephen said ...  "You stiff-necked people, uncircumcised in heart and ears, you always resist the Holy Spirit. As your fathers did, so do you. Which of the prophets did your fathers not persecute? And they killed those who announced beforehand the coming of the Righteous One, whom you have now betrayed and murdered, you who received the law as delivered by angels and did not keep it."

> Now when they heard these things they were enraged, and they ground their teeth at him. But he, full of the Holy Spirit, gazed into heaven and saw the glory of God, and Jesus standing at the right hand of God. And he said, "Behold, I see the heavens opened, and the Son of Man standing at the right hand of God." But they cried out with a loud voice and stopped their ears and rushed together at him. Then they cast him out of the city and stoned him. And the witnesses laid down their garments at the feet of a young man named Saul. And as they were stoning Stephen, he called out, "Lord Jesus, receive my spirit." And falling to his knees he cried out with a loud voice, "Lord, do not hold this sin against them." And when he had said this, he fell asleep.

> And Saul approved of his execution.

> And there arose on that day a great persecution against the church in Jerusalem, and they were all scattered throughout the regions of Judea and Samaria, except the apostles. Devout men buried Stephen and made great lamentation over him. But Saul was ravaging the church, and entering house after house, he dragged off men and women and committed them to prison. Now those who were scattered went about preaching the word.

> - Acts 6:8–7:2, 7:51-8:4 (ESV)

St. Stephen suffered the next in order. His death was occasioned by the faithful manner in which he preached the Gospel to the betrayers and murderers of Christ. To such a degree of madness were they excited, that they cast him out of the city and stoned him to death. The time when he suffered is generally supposed to have been at the passover which succeeded to that of our Lord's crucifixion, and to the era of his ascension, in the following spring.

Upon this a great persecution was raised against all who professed their belief in Christ as the Messiah, or as a prophet. We are immediately told by St. Luke, that "there was a great persecution against the church which was at Jerusalem;" and that "they were all scattered abroad throughout the regions of Judaea and Samaria, except the apostles."

About two thousand Christians, with Nicanor, one of the seven deacons, suffered martyrdom during the "persecution that arose about Stephen."
